Special relativity and steps towards general relativity lecture slides
======================================================================

(C) Boud Roukema 2011, 2014, 2024 GPL-3+

These scripts are free software: you can redistribute them and/or modify them
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

These scripts are distributed in the hope that they will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
Public License for more details. See <http://www.gnu.org/licenses/>.

Presentation pdfs for 30-hour course in special relativity and steps
towards general relativity. All materials are fully free-licensed. See
the download URLs for details of individual figures, some of which
have more permissive conditions than the default CC-BY-SA-4.0 licence.

You should be able to create the pdfs with `./make_sr.sh` and `./make_gr.sh`,
although you may have to install some standard packages such
as:
* fig2dev (e.g. the package could be called `fig2dev` or `fig2ps`)
* ghostscript
* imagemagick
* texlive-latex-base
* texlive-latex-extra
* texlive-latex-recommended
* xfig
e.g. in a Debian GNU/Linux or Debian derivative system such as Ubuntu, do
````shell
sudo aptitude install fig2dev ghostscript imagemagick \
  texlive-latex-base texlive-latex-extra texlive-latex-recommended xfig
````

Imagemagick security policy:

If you have an error such as
````
convert-im6.q16: attempt to perform an operation not allowed by the security policy `EPS' @ error/constitute.c/IsCoderAuthorized/421
````

then you will very likely have to temporarily modify your imagemagick
security policy, which will need sudo (root) access. Type
`convert -list policy` to find your current policy, and temporarily add
necessary permissions if needed. See
https://imagemagick.org/script/security-policy.php for details.
In Debian, the policy to temporarily edit could be named `/etc/ImageMagick-6/policy.xml`,
for example.

Remember to __restore__ your original security policy afterwards.
