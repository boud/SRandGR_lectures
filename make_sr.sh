#!/bin/bash

## (c) CC-BY-SA-3.0 and/or GPLv3 at your choosing
## See the Wikimedia Foundation Commons website for authorship.
## http://commons.wikimedia.org/wiki/File:Special_relativity_lecture.pdf
## http://commons.wikimedia.org/wiki/File:Steps_towards_general_relativity_lecture.pdf

## Changelog
##
## 2012-05-15 download_non_svg, etc: generalise sed expression in each
##   URL=...  because some absolute URLs are written without "http:"
##   in (at least) the Commons html files; 
## download_pdf: make ampersand, greater than, less than to appear
##   correctly in the browser-rendered version of this script instead
##   of the source version.

DATE=`date +%y%m%d`
BASE_NAME=Special_relativity_lecture
BIG_TMP_DIR=`mktemp -d /tmp/tmp_sr_XXXXXX`
PDF_FINAL=sr_${DATE}.pdf


FILES_FIG_SVG="Rotation_Euclidean1.svg Rotation_Euclidean2.svg \
Rotation_Euclidean12.svg Lorentz_rotation1.svg Lorentz_rotation2.svg \
Lorentz_rotation_ds_spacelike.svg Lorentz_rotation_ds_timelike.svg \
Lorentz_rotation_high_beta.svg Time_dilation_derivation.svg \
Space_contraction_rocket_frame.svg \
Space_contraction_observer_frame.svg SR_redshift_derivation.svg \
Add_velocity_ark_POV.svg Add_velocity_Earth_POV.svg \
Relativistic_aberration1.svg Relativistic_aberration2.svg \
Relativistic_aberration3.svg Tachyonic_antitelephone.svg \
Tachyon_observer_frame_only.svg Tachyon_rocket_frame_also.svg \
Tachyon_world_line.svg Pole_barn_Minkowski.svg \
Multiply_connected_twins_paradox_0.svg \
Multiply_connected_twins_paradox_leftmoving_POV.svg \
Multiply_connected_twins_paradox_rightmoving_POV.svg \
Multiply_connected_twins_paradox_leftmoving_cylinder.svg \
Multiply_connected_twins_paradox_rightmoving_cylinder.svg \
Multiply_connected_twins_paradox_overview.svg \
Multiply_connected_twins_paradox_time_cutpaste.svg Four-velocity.svg \
Four-momentum.svg"

## override: debug or just download/convert a few files
#FILES_FIG_SVG="Tachyonic_antitelephone.svg Multiply_connected_twins_paradox_time_cutpaste.svg"
#FILES_FIG_SVG="Add_velocity_ark_POV.svg Add_velocity_Earth_POV.svg"


FILES_OCTAVE="Beta_versus_rapidity.svg"

## debug only
#FILES_SVG="Rotation_Euclidean1.svg" 

FILES_WIKIBOOKS="Inertialoverlay.GIF Rel2.gif Rel3.gif"


FILES_SVG_ONLY="148px-Commons-logo-en.svg \
500px-World_line2.svg \
500px-Ladder_Paradox_Overview.svg \
500px-Ladder_Paradox_GarageFrame.svg \
500px-Ladder_Paradox_LadderFrame.svg \
500px-Ladder_Paradox_GarageScenario.svg \
500px-Ladder_Paradox_LadderScenario.svg \
500px-LadderParadox2_Minkowski.svg"

FILES_ANIMATIONS="Lorentz_transform_of_world_line.gif"


FILE_PDF="${BASE_NAME}.pdf"

## non-svg non-animated Commons files to be downloaded and converted
FILES_NON_SVG="NONE"

## files uploaded in .svg output format plus  .fig source code.
## * create an .eps file with math labels using the full power of LaTeX
function download_fig_svg {
    set -x # help in debugging
    FILES=$1
    DSLASH="//" ## avoid mediawiki conversion to an anchored URL
    FILES_HTML=`echo ${FILES} | sed -e "s,\([^ ]*\),https:${DSLASH}commons.wikimedia.org/wiki/File:\1,g"`
    TMP_DIR=.
    mkdir -pv ${TMP_DIR}
    cd ${TMP_DIR} || exit 1
    pwd
    wget --random-wait --wait=2 --timestamping ${FILES_HTML}
    FILES_COLON=`echo ${FILES} | sed -e 's,\([^ ]*\),File\:\1,g'`

    TMP_FILE=tmp_xx
    for file in ${FILES_COLON}; do
        ## extract .fig source
        csplit -f ${TMP_FILE} ${file} '/<.*pre>/' '{1}'
        cat ${TMP_FILE}01 |sed -e 's/<pre>//' >  tmp_yy.fig

        # Some versions may not have the "<pre>" at the beginning.
        # If necessary, search to the 'Produced by xfig version' line':
        head -n1 tmp_yy.fig > tmp_firstline
        if (grep "Produced by xfig" tmp_firstline) ; then
            # If 'Produced by xfig version' is the first line, then use the whole file:
            sed -e 's/^ *//' \
                -e 's/&#160;/ /g' -e "s/&#39;/'/g" tmp_yy.fig \
                > ${TMP_FILE}.fig
        else
            # Otherwise, split based on the first instance of 'Produced by xfig version'
            # and remove any leading spaces:
            csplit -f tmp_zz tmp_yy.fig '/Produced by xfig/'
            sed -e 's/^ *//' \
                -e 's/&#160;/ /g' -e "s/&#39;/'/g" tmp_zz01 \
                > ${TMP_FILE}.fig
            ls -l ${TMP_FILE}.fig
        fi

        ## produce a reasonable size .eps figure
        fig2dev -L pstex -F ${TMP_FILE}.fig > ${TMP_FILE}.pstex
        fig2dev -L pstex_t -p ${TMP_FILE}.pstex ${TMP_FILE}.fig \
            > ${TMP_FILE}.pstex_t

        ## start a latex-able file
        cat >${TMP_FILE}.tex <<EOF 
\documentclass[12pt]{article}
\usepackage{graphicx,color}
\pagestyle{empty}
\begin{document} 
EOF

        echo "\\input{" ${TMP_FILE}.pstex_t "}" >> ${TMP_FILE}.tex

        cat >> ${TMP_FILE}.tex <<EOF 
\end{document}
EOF

        latex ${TMP_FILE}.tex 
        dvips ${TMP_FILE}.dvi -E -o ${TMP_FILE}.eps

        ## restore file names
        FILE_BASE=`echo ${file} |sed -e 's/\.svg//'|sed -e 's/File://'`
        
        mv ${TMP_FILE}.fig ${FILE_BASE}.fig
        mv ${TMP_FILE}.eps ${FILE_BASE}.eps
        echo "The following eps file should now exist:"
        ls -l ${FILE_BASE}.eps 
        #gv  ${FILE_BASE}.eps  # uncomment this if you want to check the .eps file interactively

        ## clean up
        rm ${TMP_FILE}0[012] 
        for i in aux dvi log pstex pstex_t tex; do
            rm ${TMP_FILE}.${i}
        done
        #ls -l ${TMP_FILE}*  ## uncomment to check for anything left
    done

    set +x
}

## files uploaded in .svg output format plus octave source code
function download_octave {
    FILES=$1
    DSLASH="//" ## avoid mediawiki conversion to an anchored URL
    FILES_HTML=`echo ${FILES} | sed -e "s,\([^ ]*\),https:${DSLASH}commons.wikimedia.org/wiki/File:\1,g"`

    TMP_DIR=.
    mkdir -pv ${TMP_DIR}
    cd ${TMP_DIR} || exit 1
    pwd
    wget --random-wait --wait=2 --timestamping ${FILES_HTML}
    FILES_COLON=`echo ${FILES} | sed -e 's,\([^ ]*\),File\:\1,g'`

    TMP_FILE=tmp_xx
    for file in ${FILES_COLON}; do
        ## extract .fig source
        csplit -f ${TMP_FILE} ${file} '/<.*pre>/' '{1}'
        tail -n +2 ${TMP_FILE}01 | sed -e "s,/tmp/,${TMP_DIR}/," >  ${TMP_FILE}.m

        ## produce a reasonable size .eps figure
        octave ${TMP_FILE}.m

        ## restore file names
        FILE_BASE=`echo ${file} |sed -e 's/\.svg//'|sed -e 's/File://'`
        
        mv ${TMP_FILE}.m ${FILE_BASE}.m
        echo "The following eps file should now exist:"
        ls -l ${FILE_BASE}.eps 
        #gv  ${FILE_BASE}.eps  # uncomment this if you want to check the .eps file interactively

        ## clean up
        # no intermediate files in this case
        #ls -l ${TMP_FILE}*  ## uncomment to check for anything left
    done

}

## files available from wikibooks in various non svg formats
function download_wikibooks {
    FILES=$1
    DSLASH="//" ## avoid mediawiki conversion to an anchored URL
    FILES_HTML=`echo ${FILES} | sed -e "s,\([^ ]*\),https:${DSLASH}en.wikibooks.org/wiki/File:\1,g"`

    TMP_DIR=.
    mkdir -pv ${TMP_DIR}
    cd ${TMP_DIR} || exit 1
    pwd
    wget --random-wait --wait=2 --timestamping ${FILES_HTML}
    FILES_COLON=`echo ${FILES} | sed -e 's,\([^ ]*\),File\:\1,g'`

    for file in ${FILES_COLON}; do
        URL=`grep fullMedia ${file} | sed -e 's/.*href=\"\(\(http\|\)[^ ]*\)\".*/\1/' | sed -e "s,^${DSLASH},http:${DSLASH},"`
        wget --random-wait --wait=2 --timestamping ${URL}
        ## recover file name
        FILE_ORIG=`echo ${file} |sed -e 's/^File://'`
        FILE_BASE=`echo ${FILE_ORIG}|sed -e 's/\.[^\.]*$//'`

        ## produce a brute-force .eps figure
        convert ${FILE_ORIG} ${FILE_BASE}.eps

        echo "The following eps file should now exist:"
        ls -l ${FILE_BASE}.eps 
        #gv  ${FILE_BASE}.eps  # uncomment this if you want to check the .eps file interactively

        ## clean up
        # no intermediate files in this case
        #ls -l ${TMP_FILE}*  ## uncomment to check for anything left
    done

}

## files available from Commons in various non svg formats
function download_non_svg {
    FILES=$1
    DSLASH="//" ## avoid mediawiki conversion to an anchored URL
    FILES_HTML=`echo ${FILES} | sed -e "s,\([^ ]*\),https:${DSLASH}commons.wikimedia.org/wiki/File:\1,g"`

    TMP_DIR=.
    mkdir -pv ${TMP_DIR}
    cd ${TMP_DIR} || exit 1
    pwd
    wget --random-wait --wait=2 --timestamping ${FILES_HTML}
    FILES_COLON=`echo ${FILES} | sed -e 's,\([^ ]*\),File\:\1,g'`

    for file in ${FILES_COLON}; do
        URL=`grep fullMedia ${file} | sed -e 's/.*href=\"\(\(http\|\)[^ ]*\)\".*/\1/' | sed -e "s,^${DSLASH},http:${DSLASH},"`
        wget --random-wait --wait=2 --timestamping ${URL}
        ## recover file name
        FILE_ORIG=`echo ${file} |sed -e 's/^File://'`
        FILE_BASE=`echo ${FILE_ORIG}|sed -e 's/\.[^\.]*$//'`

        ## produce a brute-force .eps figure
        convert ${FILE_ORIG} ${FILE_BASE}.eps

        echo "The following eps file should now exist:"
        ls -l ${FILE_BASE}.eps 
        #gv  ${FILE_BASE}.eps  # uncomment this if you want to check the .eps file interactively

        ## clean up
        # no intermediate files in this case
        #ls -l ${TMP_FILE}*  ## uncomment to check for anything left
    done

}


## files available from WMCommons in .svg format that do not have
## source code available for producing them; convert locally by brute force
## * input file names must include desired px size:  148px-Commons-logo-en.svg
function download_svg_only {
    FILES=$1
    ## URLs for downloaded full html pages, pixel sizes removed from string
    DSLASH="//" ## avoid mediawiki conversion to an anchored URL
    FILES_HTML=`echo ${FILES} | sed -e "s,[0-9]\+px-\([^ ]*\),https:${DSLASH}commons.wikimedia.org/wiki/File:\1,g"`

    TMP_DIR=.
    mkdir -pv ${TMP_DIR}
    cd ${TMP_DIR} || exit 1
    pwd
    wget --random-wait --wait=2 --timestamping ${FILES_HTML}
    
    ## store pixel sizes and html file names together in each string
    FILES_COLON=`echo ${FILES} | sed -e 's,\([0-9]\+\)px-\([^ ]*\),\1:::File\:\2,g'`

    for file_colon in ${FILES_COLON}; do
        ## html file name that should now be locally available
        FILE=`echo ${file_colon} |sed -e 's/^.*::://'`
        URL=`grep -i "fileInfo.*SVG file" ${FILE} |  sed -e 's/.*href=\"\(\(http\|\)[^ ]*\)\".*/\1/' | sed -e "s,^${DSLASH},http:${DSLASH},"`
        wget --random-wait --wait=2 --timestamping ${URL}
        ## recover file name
        FILE_ORIG=`echo ${FILE} |sed -e 's/^File://'`
        PIXEL_WIDTH=`echo ${file_colon}|sed -e 's/:::.*$//'`
        FILE_BASE=`echo ${FILE_ORIG}|sed -e 's/\.svg//'`

        ## produce a brute-force .eps figure
        convert ${FILE_ORIG} -resize ${PIXEL_WIDTH} ${PIXEL_WIDTH}px-${FILE_BASE}.eps

        echo "The following eps file should now exist:"
        ls -l ${PIXEL_WIDTH}px-${FILE_BASE}.eps 
        #gv  ${FILE_BASE}.eps  # uncomment this if you want to check the .eps file interactively

        ## clean up
        # no intermediate files in this case
        #ls -l ${TMP_FILE}*  ## uncomment to check for anything left
    done

}

## animation files available from WMCommons; downloaded, not converted locally
function download_animations {
    FILES=$1
    ## URLs for downloaded full html pages
    DSLASH="//" ## avoid mediawiki conversion to an anchored URL
    FILES_HTML=`echo ${FILES} | sed -e "s,\([^ ]*\),https:${DSLASH}commons.wikimedia.org/wiki/File:\1,g"`

    TMP_DIR=.
    mkdir -pv ${TMP_DIR}
    cd ${TMP_DIR} || exit 1
    pwd
    wget --random-wait --wait=2 --timestamping ${FILES_HTML}
    
    ## name of downloaded html files
    FILES_COLON=`echo ${FILES} | sed -e 's,\([^ ]*\),File\:\1,g'`

    for file_colon in ${FILES_COLON}; do
        ## html file name that should now be locally available
        URL=`grep -i "fullMedia" ${file_colon} | sed -e 's/.*href=\"\(\(http\|\)[^ ]*\)\".*/\1/' | sed -e "s,^${DSLASH},http:${DSLASH},"`
        wget --random-wait --wait=2 --timestamping ${URL}
        ## recover file name
        FILE_ORIG=`echo ${FILE} |sed -e 's/^File://'`

        echo "The following animation file should now exist:"
        ls -l ${FILE_ORIG}
    done

}


## download the LaTeX source of the pdf file and clean it from mediawiki
## interventions.
function download_pdf {
    FILES=$1
    ## URLs for downloaded full html pages
    DSLASH="//" ## avoid mediawiki conversion to an anchored URL
    FILES_HTML=$(echo ${FILES} | sed -e "s,\([^ ]*\),https:${DSLASH}commons.wikimedia.org/wiki/File:\1,g")

    TMP_DIR=.
    mkdir -pv ${TMP_DIR}
    cd ${TMP_DIR} || exit 1
    pwd
    wget --random-wait --wait=2 --timestamping ${FILES_HTML}
    
    ## name of downloaded html files
    FILES_COLON=$(echo ${FILES} | sed -e 's,\([^ ]*\),File\:\1,g')

    for file_colon in ${FILES_COLON}; do
        ## html file name that should now be locally available
        csplit ${file_colon} '/<.*pre>/' '{*}'
        tail -n +2 xx03 | \
            sed -e 's/>Template:\([^<]*\)</>{{\1}}</g' \
                -e 's,<a href[^>]*>,,gi' -e 's,</a>,,gi' \
                -e 's/&amp;amp;/\&/gi' \
                -e 's/&amp;lt;/</gi' -e 's/&amp;gt;/>/gi' \
                -e 's/&lt;/</gi' -e 's/&gt;/>/gi' \
                -e 's/&#160;/ /g' \
            > xx03.tex
        mv -v --backup xx03.tex ${BASE_NAME}.tex

        echo "The following pdf file should now exist:"
        ls -l ${BASE_NAME}.tex
    done

}

# produce the .pdf file
# WARNING: the intermediate postscript files may be big, so 
# make sure your have plenty of space in ${BIG_TMP_DIR}
function produce_pdf {
    set -x
    BASE_NAME=$1
    BIG_TMP_DIR=$2
    PDF_FINAL=$3
    printf "\n\n\n========== Will run LaTeX... ============\n\n\n"
    latex ${BASE_NAME}.tex
    printf "\n\n\n========== Will run LaTeX again... ============\n\n\n"
    latex ${BASE_NAME}.tex
    printf "\n\n\n========== Will run dvips... ============\n\n\n"
    dvips ${BASE_NAME}.dvi -o ${BIG_TMP_DIR}/${BASE_NAME}.ps
    printf "\n\n\n========== Will run ps2pdf... ============\n\n\n"
    # May need the following as a temporary hack around 2022/2023:
    # https://github.com/GenericMappingTools/gmt/issues/4453
    sed -e 's/\.setopacityalpha/.setfillconstantalpha/g' ${BIG_TMP_DIR}/${BASE_NAME}.ps > ${BIG_TMP_DIR}/${BASE_NAME}.eps
    ps2pdf -dALLOWPSTRANSPARENCY -dNORANGEPAGESIZE ${BIG_TMP_DIR}/${BASE_NAME}.eps ${BIG_TMP_DIR}/${BASE_NAME}.pdf
    cp -pi ${BIG_TMP_DIR}/${BASE_NAME}.pdf ${PDF_FINAL}
    echo " "
    echo "You should now have the pdf file:"
    ls -l ${PDF_FINAL}
    echo "You should keep this file and " ${FILES_ANIMATIONS} 
    echo " "
    echo "The other files can be safely removed, including those"
    echo "in ${BIG_TMP_DIR}/":
    ls -l ${BIG_TMP_DIR}
    set +x    
}

# interactive check of licences after downloading 
function check_licences {
    for i in `ls File\:*`; do 
        egrep -H --color "(is licensed.*Creative Commons.*/by-sa/|.*release.*public domain|Permission is granted.*GNU Free Documentation License)" $i
    done 

    echo "The following files might have problems. Check manually."  ## -L = files without
    for i in `ls File\:*`; do 
        egrep -L -H --color "(is licensed.*Creative Commons.*/by-sa/|.*release.*public domain|Permission is granted.*GNU Free Documentation License)" $i
    done 
}


download_octave "${FILES_OCTAVE}"

download_fig_svg "${FILES_FIG_SVG}"

download_wikibooks "${FILES_WIKIBOOKS}"

###download_non_svg "${FILES_NON_SVG}"

download_svg_only "${FILES_SVG_ONLY}"

download_animations "${FILES_ANIMATIONS}"

### 2024-03-17 Cleaning the source from the Wikimedia Commons download is not so
### easy to maintain for a full .tex file, so for the moment, use the .tex file
### in the git repository and comment out this download function:
# download_pdf ${FILE_PDF}

produce_pdf ${BASE_NAME} ${BIG_TMP_DIR} ${PDF_FINAL}

check_licences  ## prior to uploading a new .pdf

